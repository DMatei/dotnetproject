﻿using System.Collections.Generic;
using System.Linq;
using FiiTT.Core;
using FiiTT.Core.Interfaces;
using FiiTT.Infrastructure.Data;

namespace FiiTT.Infrastructure
{
    public class TeacherRepository : ITeacherRepository
    {
        private readonly ApplicationDbContext _context = new ApplicationDbContext();

        public void Add(Teacher t)
        {
            _context.Teachers.Add(t);
            _context.SaveChanges();
        }

        public void Edit(Teacher t)
        {
            _context.Set<Teacher>().Update(t);
            _context.SaveChanges();
        }

        public void Remove(string email)
        {
            var t = _context.Teachers.Find(email);
            _context.Teachers.Remove(t);
            _context.SaveChanges();
        }

        public IEnumerable<Teacher> GetTeachers()
        {
            return _context.Teachers;
        }

        public Teacher FindByEmail(string email)
        {
            var t = (from r in _context.Teachers where r.Email == email select r).FirstOrDefault();
            return t;
        }
    }
}