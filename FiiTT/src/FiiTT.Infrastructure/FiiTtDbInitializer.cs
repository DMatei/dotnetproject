﻿using System;
using System.Linq;
using FiiTT.Core.EnumTypes;
using FiiTT.Infrastructure.Data;

namespace FiiTT.Infrastructure
{
    public class FiiTtDbInitializer
    {
        public static void Intialize(ApplicationDbContext context)
        {
            context.Database.EnsureCreated();

            if (context.Users.Any())
                return; // DB has been seeded

            const string hashedPassword =
                "AQAAAAEAACcQAAAAEB8DeKAzjb3sfQk6ff60DswEMM0dWJA5ADpB4/5/s2o4pUXJy41niNDu0rXZHgjUcQ==";
            // ↑ Password: Admin-NET1

            var users = new[]
            {
                new ApplicationUser
                {
                    UserName = "admin1@gmail.com",
                    Email = "admin1@gmail.com",
                    IsValid = true,
                    LockoutEnabled = true,
                    NormalizedEmail = "ADMIN1@GMAIL.COM",
                    NormalizedUserName = "ADMIN1@GMAIL.COM",
                    PhoneNumberConfirmed = false,
                    PasswordHash = hashedPassword,
                    SecurityStamp = Guid.NewGuid().ToString(),
                    UserTypes = UserType.Admin
                },
                new ApplicationUser
                {
                    UserName = "admin2@gmail.com",
                    Email = "admin2@gmail.com",
                    IsValid = true,
                    LockoutEnabled = true,
                    NormalizedEmail = "ADMIN2@GMAIL.COM",
                    NormalizedUserName = "ADMIN2@GMAIL.COM",
                    PhoneNumberConfirmed = false,
                    PasswordHash = hashedPassword,
                    SecurityStamp = Guid.NewGuid().ToString(),
                    UserTypes = UserType.Admin
                },
                new ApplicationUser
                {
                    UserName = "admin3@gmail.com",
                    Email = "admin3@gmail.com",
                    IsValid = true,
                    LockoutEnabled = true,
                    NormalizedEmail = "ADMIN3@GMAIL.COM",
                    NormalizedUserName = "ADMIN3@GMAIL.COM",
                    PhoneNumberConfirmed = false,
                    PasswordHash = hashedPassword,
                    SecurityStamp = Guid.NewGuid().ToString(),
                    UserTypes = UserType.Admin
                },
                new ApplicationUser
                {
                    UserName = "admin4@gmail.com",
                    Email = "admin4@gmail.com",
                    IsValid = true,
                    LockoutEnabled = true,
                    NormalizedEmail = "ADMIN4@GMAIL.COM",
                    NormalizedUserName = "ADMIN4@GMAIL.COM",
                    PhoneNumberConfirmed = false,
                    PasswordHash = hashedPassword,
                    SecurityStamp = Guid.NewGuid().ToString(),
                    UserTypes = UserType.Admin
                },
                new ApplicationUser
                {
                    UserName = "admin5@gmail.com",
                    Email = "admin5@gmail.com",
                    IsValid = true,
                    LockoutEnabled = true,
                    NormalizedEmail = "ADMIN5@GMAIL.COM",
                    NormalizedUserName = "ADMIN5@GMAIL.COM",
                    PhoneNumberConfirmed = false,
                    PasswordHash = hashedPassword,
                    SecurityStamp = Guid.NewGuid().ToString(),
                    UserTypes = UserType.Admin
                },
                new ApplicationUser
                {
                    UserName = "admin6@gmail.com",
                    Email = "admin6@gmail.com",
                    IsValid = true,
                    LockoutEnabled = true,
                    NormalizedEmail = "ADMIN6@GMAIL.COM",
                    NormalizedUserName = "ADMIN6@GMAIL.COM",
                    PhoneNumberConfirmed = false,
                    PasswordHash = hashedPassword,
                    SecurityStamp = Guid.NewGuid().ToString(),
                    UserTypes = UserType.Admin
                },
                new ApplicationUser
                {
                    UserName = "admin7@gmail.com",
                    Email = "admin7@gmail.com",
                    IsValid = true,
                    LockoutEnabled = true,
                    NormalizedEmail = "ADMIN7@GMAIL.COM",
                    NormalizedUserName = "ADMIN7@GMAIL.COM",
                    PhoneNumberConfirmed = false,
                    PasswordHash = hashedPassword,
                    SecurityStamp = Guid.NewGuid().ToString(),
                    UserTypes = UserType.Admin
                },
                new ApplicationUser
                {
                    UserName = "admin8@gmail.com",
                    Email = "admin8@gmail.com",
                    IsValid = true,
                    LockoutEnabled = true,
                    NormalizedEmail = "ADMIN8@GMAIL.COM",
                    NormalizedUserName = "ADMIN8@GMAIL.COM",
                    PhoneNumberConfirmed = false,
                    PasswordHash = hashedPassword,
                    SecurityStamp = Guid.NewGuid().ToString(),
                    UserTypes = UserType.Admin
                },
                new ApplicationUser
                {
                    UserName = "admin9@gmail.com",
                    Email = "admin9@gmail.com",
                    IsValid = true,
                    LockoutEnabled = true,
                    NormalizedEmail = "ADMIN9@GMAIL.COM",
                    NormalizedUserName = "ADMIN9@GMAIL.COM",
                    PhoneNumberConfirmed = false,
                    PasswordHash = hashedPassword,
                    SecurityStamp = Guid.NewGuid().ToString(),
                    UserTypes = UserType.Admin
                },
                new ApplicationUser
                {
                    UserName = "admin10@gmail.com",
                    Email = "admin10@gmail.com",
                    IsValid = true,
                    LockoutEnabled = true,
                    NormalizedEmail = "ADMIN10@GMAIL.COM",
                    NormalizedUserName = "ADMIN10@GMAIL.COM",
                    PhoneNumberConfirmed = false,
                    PasswordHash = hashedPassword,
                    SecurityStamp = Guid.NewGuid().ToString(),
                    UserTypes = UserType.Admin
                }
            };
            foreach (var i in users)
                context.Users.Add(i);

            context.SaveChanges();
        }
    }
}