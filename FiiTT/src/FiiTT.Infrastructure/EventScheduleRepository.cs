﻿using System;
using System.Collections.Generic;
using System.Linq;
using FiiTT.Core;
using FiiTT.Core.Interfaces;
using FiiTT.Infrastructure.Data;

namespace FiiTT.Infrastructure
{
    public class EventScheduleRepository : IEventScheduleRepository
    {
        private readonly ApplicationDbContext _context = new ApplicationDbContext();

        public void Add(EventSchedule e)
        {
            _context.EventSchedules.Add(e);
            _context.SaveChanges();
        }

        public void Edit(EventSchedule e)
        {
            _context.Set<EventSchedule>().Update(e);
            _context.SaveChanges();
        }

        public void Remove(Guid eventScheduleId)
        {
            var e = _context.EventSchedules.Find(eventScheduleId);
            _context.EventSchedules.Remove(e);
            _context.SaveChanges();
        }

        public IEnumerable<EventSchedule> GetEventSchedules()
        {
            return _context.EventSchedules;
        }

        public EventSchedule FindById(Guid eventScheduleId)
        {
            var e = (from r in _context.EventSchedules where r.EventScheduleId == eventScheduleId select r).FirstOrDefault();
            return e;
        }
    }
}