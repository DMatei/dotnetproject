﻿using FiiTT.Core;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FiiTT.Infrastructure.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Classroom> Classrooms { get; set; }

        public DbSet<Group> Groups { get; set; }

        public DbSet<Class> Classes { get; set; }

        public DbSet<Schedule> Schedules { get; set; }

        public DbSet<Teacher> Teachers { get; set; }

        public DbSet<TeacherToClass> TeachersToClasses { get; set; }

        public DbSet<EventSchedule> EventSchedules { get; set; }

        public DbSet<ExamSchedule> ExamSchedules { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer(
                @"Server=(localdb)\MSSQLLocalDB;Database=FiiTT-DB;Trusted_Connection=True;MultipleActiveResultSets=True");
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<Classroom>()
                .Property(c => c.ClassroomId)
                .IsRequired();
            builder.Entity<Classroom>()
                .Property(c => c.ClassroomName)
                .IsRequired()
                .HasMaxLength(10);
            builder.Entity<Classroom>()
                .Property(c => c.ClassroomType)
                .IsRequired();
            
            builder.Entity<Group>()
                .Property(g => g.GroupId)
                .IsRequired();
            builder.Entity<Group>()
                .Property(g => g.HalfYearName)
                .IsRequired();
            builder.Entity<Group>()
                .Property(g => g.YearNumber)
                .IsRequired();

            builder.Entity<Class>()
                .Property(o => o.ClassId)
                .IsRequired();
            builder.Entity<Class>()
                .Property(o => o.FrequencyType)
                .IsRequired();
            builder.Entity<Class>()
                .Property(o => o.ClassName)
                .IsRequired()
                .HasMaxLength(50);
            builder.Entity<Class>()
                .Property(o => o.ShortName)
                .IsRequired()
                .HasMaxLength(10);
            builder.Entity<Class>()
                .Property(o => o.Semester)
                .IsRequired();

            builder.Entity<Schedule>()
                .Property(s => s.ClassroomName)
                .IsRequired();
            builder.Entity<Schedule>()
                .Property(s => s.GroupName)
                .IsRequired();
            builder.Entity<Schedule>()
                .Property(s => s.ScheduleId)
                .IsRequired();
            builder.Entity<Schedule>()
                .Property(s => s.TeacherName)
                .IsRequired();
            builder.Entity<Schedule>()
                .Property(s => s.ClassName)
                .IsRequired()
                .HasMaxLength(50);
            builder.Entity<Schedule>()
                .Property(s => s.StartSchedule)
                .IsRequired();
            builder.Entity<Schedule>()
                .Property(s => s.EndSchedule)
                .IsRequired();
            builder.Entity<Schedule>()
                .Property(s => s.ClassType)
                .IsRequired();
            builder.Entity<Schedule>()
                .Property(s => s.Day)
                .IsRequired();

            builder.Entity<Teacher>()
                .Property(t => t.Email)
                .IsRequired();
            builder.Entity<Teacher>()
                .Property(t => t.TeacherType)
                .IsRequired();
            builder.Entity<Teacher>()
                .Property(t => t.FirstName)
                .IsRequired()
                .HasMaxLength(30);
            builder.Entity<Teacher>()
                .Property(t => t.LastName)
                .IsRequired()
                .HasMaxLength(30);

            builder.Entity<TeacherToClass>()
                .Property(tto => tto.ClassId)
                .IsRequired();
            builder.Entity<TeacherToClass>()
                .Property(tto => tto.TeacherId)
                .IsRequired();
            
            builder.Entity<EventSchedule>()
                .Property(s => s.EventType)
                .IsRequired();
            builder.Entity<EventSchedule>()
                .Property(s => s.ClassroomName)
                .IsRequired();
            builder.Entity<EventSchedule>()
                .Property(s => s.Date)
                .IsRequired();
            builder.Entity<EventSchedule>()
                .Property(s => s.StartEvent)
                .IsRequired();
            builder.Entity<EventSchedule>()
                .Property(s => s.EndEvent)
                .IsRequired();
            builder.Entity<EventSchedule>()
                .Property(s => s.EventName)
                .IsRequired();
            builder.Entity<EventSchedule>()
                .Property(s => s.GroupName)
                .IsRequired();
            builder.Entity<EventSchedule>()
                .Property(s => s.Organizer)
                .IsRequired();

            builder.Entity<ExamSchedule>()
                .Property(s => s.ExamType)
                .IsRequired();
            builder.Entity<ExamSchedule>()
                .Property(s => s.ClassroomName)
                .IsRequired();
            builder.Entity<ExamSchedule>()
                .Property(s => s.Date)
                .IsRequired();
            builder.Entity<ExamSchedule>()
                .Property(s => s.StartExam)
                .IsRequired();
            builder.Entity<ExamSchedule>()
                .Property(s => s.EndExam)
                .IsRequired();
            builder.Entity<ExamSchedule>()
                .Property(s => s.ClassName)
                .IsRequired();
            builder.Entity<ExamSchedule>()
                .Property(s => s.GroupName)
                .IsRequired();
            builder.Entity<ExamSchedule>()
                .Property(s => s.TeacherName)
                .IsRequired();
        }
    }
}