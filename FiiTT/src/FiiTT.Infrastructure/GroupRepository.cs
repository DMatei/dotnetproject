﻿using System;
using System.Collections.Generic;
using System.Linq;
using FiiTT.Core;
using FiiTT.Core.Interfaces;
using FiiTT.Infrastructure.Data;

namespace FiiTT.Infrastructure
{
    public class GroupRepository : IGroupRepository
    {
        private readonly ApplicationDbContext _context = new ApplicationDbContext();

        public void Add(Group g)
        {
            _context.Groups.Add(g);
        }

        public void Edit(Group g)
        {
            _context.Set<Group>().Update(g);
            _context.SaveChanges();
        }

        public void Remove(Guid groupId)
        {
            var g = _context.Groups.Find(groupId);
            _context.Groups.Remove(g);
            _context.SaveChanges();
        }

        public IEnumerable<Group> GetGroups()
        {
            return _context.Groups;
        }

        public Group FindById(Guid groupId)
        {
            var g = (from r in _context.Groups where r.GroupId == groupId select r).FirstOrDefault();
            return g;
        }
    }
}