﻿using System;
using System.Collections.Generic;
using System.Linq;
using FiiTT.Core;
using FiiTT.Core.Interfaces;
using FiiTT.Infrastructure.Data;

namespace FiiTT.Infrastructure
{
    public class ExamScheduleRepository : IExamScheduleRepository
    {
        private readonly ApplicationDbContext _context = new ApplicationDbContext();

        public void Add(ExamSchedule e)
        {
            _context.ExamSchedules.Add(e);
            _context.SaveChanges();
        }

        public void Edit(ExamSchedule e)
        {
            _context.Set<ExamSchedule>().Update(e);
            _context.SaveChanges();
        }

        public void Remove(Guid examScheduleId)
        {
            var e = _context.ExamSchedules.Find(examScheduleId);
            _context.ExamSchedules.Remove(e);
            _context.SaveChanges();
        }

        public IEnumerable<ExamSchedule> GetExamSchedules()
        {
            return _context.ExamSchedules;
        }

        public ExamSchedule FindById(Guid examScheduleId)
        {
            var e = (from r in _context.ExamSchedules where r.ExamScheduleId == examScheduleId select r).FirstOrDefault();
            return e;
        }
    }
}