﻿using FiiTT.Core.EnumTypes;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace FiiTT.Infrastructure
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public virtual UserType UserTypes { get; set; }
        public virtual bool IsValid { get; set; }
    }
}