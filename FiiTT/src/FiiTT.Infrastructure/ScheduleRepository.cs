﻿using System;
using System.Collections.Generic;
using System.Linq;
using FiiTT.Core;
using FiiTT.Core.Interfaces;
using FiiTT.Infrastructure.Data;

namespace FiiTT.Infrastructure
{
    public class ScheduleRepository : IScheduleRepository
    {
        private readonly ApplicationDbContext _context = new ApplicationDbContext();

        public void Add(Schedule s)
        {
            _context.Schedules.Add(s);
            _context.SaveChanges();
        }

        public void Edit(Schedule s)
        {
            _context.Set<Schedule>().Update(s);
            _context.SaveChanges();
        }

        public void Remove(Guid scheduleId)
        {
            var s = _context.Schedules.Find(scheduleId);
            _context.Schedules.Remove(s);
            _context.SaveChanges();
        }

        public IEnumerable<Schedule> GetSchedules()
        {
            return _context.Schedules;
        }

        public Schedule FindById(Guid scheduleId)
        {
            var s = (from r in _context.Schedules where r.ScheduleId == scheduleId select r).FirstOrDefault();
            return s;
        }
    }
}