﻿using System;
using System.Collections.Generic;
using System.Linq;
using FiiTT.Core;
using FiiTT.Core.Interfaces;
using FiiTT.Infrastructure.Data;

namespace FiiTT.Infrastructure
{
    public class TeacherToClassRepository : ITeacherToClassRepository
    {
        private readonly ApplicationDbContext _context = new ApplicationDbContext();

        public void Add(TeacherToClass t)
        {
            _context.TeachersToClasses.Add(t);
            _context.SaveChanges();
        }

        public void Edit(TeacherToClass t)
        {
            _context.Set<TeacherToClass>().Update(t);
            _context.SaveChanges();
        }

        public void Remove(Guid teacherToClassId)
        {
            var t = _context.TeachersToClasses.Find(teacherToClassId);
            _context.TeachersToClasses.Remove(t);
            _context.SaveChanges();
        }

        public IEnumerable<TeacherToClass> GetTeacherToClasses()
        {
            return _context.TeachersToClasses;
        }

        public TeacherToClass FindById(Guid teacherToClassId)
        {
            var t =
                (from r in _context.TeachersToClasses where r.TeacherToClassId == teacherToClassId select r)
                .FirstOrDefault();
            return t;
        }
    }
}