﻿using System;
using System.Collections.Generic;
using System.Linq;
using FiiTT.Core;
using FiiTT.Core.Interfaces;
using FiiTT.Infrastructure.Data;

namespace FiiTT.Infrastructure
{
    public class ClassroomRepository : IClassroomRepository
    {
        private readonly ApplicationDbContext _context = new ApplicationDbContext();

        public void Add(Classroom c)
        {
            _context.Classrooms.Add(c);
            _context.SaveChanges();
        }

        public void Edit(Classroom c)
        {
            _context.Set<Classroom>().Update(c);
            _context.SaveChanges();
        }

        public void Remove(Guid classroomId)
        {
            var c = _context.Classrooms.Find(classroomId);
            _context.Classrooms.Remove(c);
            _context.SaveChanges();
        }

        public IEnumerable<Classroom> GetClassrooms()
        {
            return _context.Classrooms;
        }

        public Classroom FindById(Guid classroomId)
        {
            var c = (from r in _context.Classrooms where r.ClassroomId == classroomId select r).FirstOrDefault();
            return c;
        }
    }
}