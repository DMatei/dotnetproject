﻿using System;
using System.Collections.Generic;
using System.Linq;
using FiiTT.Core;
using FiiTT.Core.Interfaces;
using FiiTT.Infrastructure.Data;

namespace FiiTT.Infrastructure
{
    public class ClassRepository : IClassRepository
    {
        private readonly ApplicationDbContext _context = new ApplicationDbContext();

        public void Add(Class o)
        {
            _context.Classes.Add(o);
            _context.SaveChanges();
        }

        public void Edit(Class o)
        {
            _context.Set<Class>().Update(o);
            _context.SaveChanges();
        }

        public void Remove(Guid classId)
        {
            var o = _context.Classes.Find(classId);
            _context.Classes.Remove(o);
            _context.SaveChanges();
        }

        public IEnumerable<Class> GetClasses()
        {
            return _context.Classes;
        }

        public Class FindById(Guid classId)
        {
            var o = (from r in _context.Classes where r.ClassId == classId select r).FirstOrDefault();
            return o;
        }
    }
}