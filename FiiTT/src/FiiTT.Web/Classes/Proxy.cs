﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FiiTT.Core.EnumTypes;
using FiiTT.Infrastructure;
using FiiTT.Infrastructure.Data;
using FiiTT.Web.Controllers;

namespace FiiTT.Web
{
    using System;
    using System.IdentityModel.Tokens.Jwt;
    using System.Security.Claims;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.IdentityModel.Tokens;

    namespace OpenGameList.Classes
    {
        public class JwtProvider
        {
            private readonly RequestDelegate _next;

            // JWT-related members
            private TimeSpan _tokenExpiration;
            private DateTime tokenMaxTimeOnPage;
            private readonly SigningCredentials _signingCredentials;
            private UserType? currentUserType;
            private readonly UserManager<ApplicationUser> _userManager;

            private static readonly string PrivateKey = "private_key_1234567890";
            public static readonly SymmetricSecurityKey SecurityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(PrivateKey));
            public static readonly string Issuer = "FiiTTApp";
            public static string TokenEndPoint = "/api/connect/token";

            public JwtProvider(
                RequestDelegate next,
                UserManager<ApplicationUser> userManager)
            {
                _next = next;
                _tokenExpiration = TimeSpan.FromMinutes(2);
                _signingCredentials = new SigningCredentials(SecurityKey, SecurityAlgorithms.HmacSha256);
                _userManager = userManager;
                currentUserType = null;
            }

            public Task Invoke(HttpContext httpContext)
            {
                if (httpContext.Request.Method.Equals("POST") && httpContext.Request.HasFormContentType)
                {
                    try
                    {
                        if (currentUserType == null)
                        {
                            string email = httpContext.Request.Form["email"];
                            currentUserType = _userManager.Users.First(u => u.Email == email).UserTypes;
                            tokenMaxTimeOnPage = DateTime.Now.AddHours(1);
                        }
                        TimeSpan difference = DateTime.Now.Subtract(tokenMaxTimeOnPage);
                        if (difference > TimeSpan.FromSeconds(1))
                        {
                            currentUserType = null;
                            UserTypeValidation.SetUserTypeDelegate();
                        }
                    }
                    catch (Exception)
                    {
                        return _next(httpContext);
                    }
                }
                return _next(httpContext);
            }
           
        }

        // Extension method used to add the middleware to the HTTP request pipeline.
        public static class JwtProviderExtensions
        {
            public static IApplicationBuilder UseJwtProvider(this IApplicationBuilder builder)
            {
                return builder.UseMiddleware<JwtProvider>();
            }
        }
    }

}
