﻿using System;
using System.Threading.Tasks;
using FiiTT.Core.EnumTypes;

namespace FiiTT.Web
{
    public class UserTypeValidation
    {
        public delegate UserType? TypeDelegate();
        public delegate Task SetTypeDelegate();
        public static TypeDelegate UserTypeDelegate;
        public static SetTypeDelegate SetUserTypeDelegate;

        public bool HasRight(UserType? type)
        {
            bool isCurrentUserRightType;
            try
            {
                isCurrentUserRightType = HasPermission(type);
            }
            catch (Exception)
            {
                return false;
            }
            return isCurrentUserRightType;
        }

        private bool HasPermission(UserType? type)
        {
            return UserTypeDelegate() == type;
        }
    }
}