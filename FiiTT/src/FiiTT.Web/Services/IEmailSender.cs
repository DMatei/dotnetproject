﻿using System.Threading.Tasks;

namespace FiiTT.Web.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}