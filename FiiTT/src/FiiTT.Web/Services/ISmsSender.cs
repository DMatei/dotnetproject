﻿using System.Threading.Tasks;

namespace FiiTT.Web.Services
{
    public interface ISmsSender
    {
        Task SendSmsAsync(string number, string message);
    }
}