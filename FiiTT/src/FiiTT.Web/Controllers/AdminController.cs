using System;
using System.Linq;
using System.Threading.Tasks;
using FiiTT.Core;
using FiiTT.Core.EnumTypes;
using FiiTT.Infrastructure;
using FiiTT.Infrastructure.Data;
using FiiTT.Web.Models.ClassroomViewModels;
using FiiTT.Web.Models.ClassViewModels;
using FiiTT.Web.Models.EventScheduleViewModels;
using FiiTT.Web.Models.ExamScheduleViewModels;
using FiiTT.Web.Models.GroupViewModels;
using FiiTT.Web.Models.ScheduleViewModels;
using FiiTT.Web.Models.TeacherToClassViewModels;
using FiiTT.Web.Models.TeacherViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Class = FiiTT.Core.Class;

namespace FiiTT.Web.Controllers
{
    public class AdminController : Controller
    {
        private readonly ApplicationDbContext _context;

        public AdminController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Teachers
        public ActionResult Index()
        {
            if (!new UserTypeValidation().HasRight(UserType.Admin))
                return RedirectToAction(nameof(HomeController.Index), "Home");

            ViewBag.TeachersList = _context.Teachers.OrderBy(g => g.Email);
            ViewBag.ClassroomsList = _context.Classrooms.OrderBy(c => c.ClassroomType).ThenBy(c => c.ClassroomName);
            ViewBag.ClassesList = _context.Classes.OrderBy(o => o.ClassName);
            ViewBag.GroupsList = _context.Groups.OrderBy(g => g.YearNumber).ThenBy(g => g.HalfYearName).ThenBy(g => g.GroupNumber);
            ViewBag.ScheduleList = _context.Schedules.OrderBy(s => s.Day).ThenBy(s => s.StartSchedule).ThenBy(s => s.EndSchedule).ThenBy(s => s.ClassroomName);
            ViewBag.EventSchedulesList = _context.EventSchedules.OrderBy(e => e.Date).ThenBy(e => e.StartEvent).ThenBy(e => e.EndEvent);
            ViewBag.ExamSchedulesList = _context.ExamSchedules.OrderBy(e => e.Date).ThenBy(e => e.StartExam).ThenBy(e => e.EndExam);
            ViewBag.TeachersToClassesList = from teachersToClasses in _context.TeachersToClasses
                join
                teachers in _context.Teachers on teachersToClasses.TeacherId equals teachers.TeacherId
                join
                classes in _context.Classes on teachersToClasses.ClassId equals classes.ClassId
                select new AddTeacherToClassViewModel { TeacherToClassId = teachersToClasses.TeacherToClassId, TeacherName = teachers.LastName + " " + teachers.FirstName, ClassName = classes.ClassName};
            
            var teacherTypeList = from TeacherType t in Enum.GetValues(typeof(TeacherType))
                select new {Id = t, Name = t.ToString()};
            ViewData["TeacherType"] = new SelectList(teacherTypeList, "Id", "Name");

            var classroomTypeList = from ClassroomType c in Enum.GetValues(typeof(ClassroomType))
                select new {Id = c, Name = c.ToString()};
            var selectClassroomsList = new SelectList(classroomTypeList, "Id", "Name");
            for (var j = 0; j < selectClassroomsList.Count(); j++)
            {
                var result = selectClassroomsList.ElementAt(j).Value;
                selectClassroomsList.ElementAt(j).Text =
                    string.Concat(result.Select((x, i) => (i > 0) && char.IsUpper(x) ? " " + x.ToString() : x.ToString()));
            }
            ViewData["ClassroomType"] = selectClassroomsList;

            var classTypeList = from ClassType obj in Enum.GetValues(typeof(ClassType))
                select new {Id = obj, Name = obj.ToString()};
            ViewData["ClassType"] = new SelectList(classTypeList, "Id", "Name");

            var frequencyTypeList = from FrequencyType f in Enum.GetValues(typeof(FrequencyType))
                                  select new { Id = f, Name = f.ToString() };
            ViewData["FrequencyType"] = new SelectList(frequencyTypeList, "Id", "Name");

            var eventsTypeList = from EventType e in Enum.GetValues(typeof(EventType))
                                  select new { Id = e, Name = e.ToString() };
            var selectEventsList = new SelectList(eventsTypeList, "Id", "Name");
            for (var j = 0; j < selectEventsList.Count(); j++)
            {
                var result = selectEventsList.ElementAt(j).Value;
                selectEventsList.ElementAt(j).Text =
                    string.Concat(result.Select((x, i) => (i > 0) && char.IsUpper(x) ? " " + x.ToString() : x.ToString()));
            }
            ViewData["EventType"] = selectEventsList;

            var examsTypeList = from ExamType e in Enum.GetValues(typeof(ExamType))
                                 select new { Id = e, Name = e.ToString() };
            var selectExamsList = new SelectList(examsTypeList, "Id", "Name");
            for (var j = 0; j < selectExamsList.Count(); j++)
            {
                var result = selectExamsList.ElementAt(j).Value;
                selectExamsList.ElementAt(j).Text =
                    string.Concat(result.Select((x, i) => (i > 0) && char.IsUpper(x) ? " " + x.ToString() : x.ToString()));
            }
            ViewData["ExamType"] = selectExamsList;

            var yearsList = from Years y in Enum.GetValues(typeof(Years))
                                  select new { Id = (int)y, Name = (int)y };
            ViewData["YearsList"] = new SelectList(yearsList, "Id", "Name");

            var halfYearsList = from HalfYears hy in Enum.GetValues(typeof(HalfYears))
                            select new { Id = hy, Name = hy.ToString() };
            ViewData["HalfYearsList"] = new SelectList(halfYearsList, "Id", "Name");

            var daysList = from Days d in Enum.GetValues(typeof(Days))
                            select new { Id = d, Name = d.ToString() };
            ViewData["DaysList"] = new SelectList(daysList, "Id", "Name");

            var teacherNameList = from teachers in _context.Teachers
                                  select new { Id = teachers.TeacherId, Name = teachers.LastName + " " + teachers.FirstName };
            ViewData["TeacherName"] = new SelectList(teacherNameList, "Id", "Name");

            var classNameList = from classes in _context.Classes
                                  select new { Id = classes.ClassId, Name = classes.ClassName };
            ViewData["ClassName"] = new SelectList(classNameList, "Id", "Name");

            var groupNameList = from groups in _context.Groups
                                 select new { Id = groups.GroupId, Name = groups.YearNumber + groups.HalfYearName + string.Concat(groups.GroupNumber, string.Empty)};       
            ViewData["GroupName"] = new SelectList(groupNameList, "Id", "Name");

            return View();
        }

        #region Teacher

        // GET: Admin/AddTeacher
        public IActionResult AddTeacher()
        {
            return View();
        }

        // POST: Admin/AddTeacher
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddTeacher(
            [Bind("Email,FirstName,LastName,TeacherType")] AddTeacherViewModel addTeacherViewModel)
        {

            if (ModelState.IsValid)
            {
                var newTeacher = new Teacher(addTeacherViewModel.Email, addTeacherViewModel.TeacherType,
                    addTeacherViewModel.FirstName, addTeacherViewModel.LastName);

                var newTeacherUser = new ApplicationUser
                {
                    UserName = addTeacherViewModel.Email,
                    Email = addTeacherViewModel.Email,
                    IsValid = true,
                    LockoutEnabled = true,
                    NormalizedEmail = addTeacherViewModel.Email.ToUpper(),
                    NormalizedUserName = addTeacherViewModel.Email.ToUpper(),
                    PhoneNumberConfirmed = false,
                    PasswordHash =
                        "AQAAAAEAACcQAAAAEMX8rrFlv8cRNPjOBeLPaAttaVaeepTuqvlgHrIt2GSuovlHK7ClZFortvPqRmCF3w==",
                    // ? Password: 12#qwE
                    SecurityStamp = Guid.NewGuid().ToString(),
                    UserTypes = UserType.Teacher
                };

                var result = _context.Users.Any(t => t.Email == newTeacher.Email);
                if(result)
                    return NoContent();

                _context.Teachers.Add(newTeacher);
                _context.Users.Add(newTeacherUser);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return NoContent();
        }

        // GET: Admin/DeleteTeacher
        public async Task<IActionResult> DeleteTeacher(string id)
        {
            if (_context == null)
                return NotFound();

            var teacher = await _context.Teachers.SingleOrDefaultAsync(m => m.Email == id);
            if (teacher == null)
                return NotFound();

            return View();
        }

        // POST: Admin/DeleteTeacher
        [HttpPost]
        [ActionName("DeleteTeacher")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteTeacherConfirmed(string id)
        {
            var teacher = await _context.Teachers.SingleOrDefaultAsync(m => m.Email == id);
            var teacherAccount = await _context.Users.SingleOrDefaultAsync(m => m.Email == id);
            _context.Teachers.Remove(teacher);
            _context.Users.Remove(teacherAccount);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        #endregion

        #region Classroom

        public IActionResult AddClassroom()
        {
            return View();
        }

        // POST: Admin/AddClassroom
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddClassroom([Bind("ClassroomType,ClassroomName")] AddClassroomViewModel addClassroomViewModel)
        {
            if (ModelState.IsValid)
            {
                var newClassroom = new Classroom(addClassroomViewModel.ClassroomType, addClassroomViewModel.ClassroomName);

                var result = _context.Classrooms.Any(c => c.ClassroomName == newClassroom.ClassroomName);
                if (result)
                    return NoContent();

                _context.Classrooms.Add(newClassroom);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return NoContent();
        }

        // GET: Admin/DeleteClassroom
        public async Task<IActionResult> DeleteClassroom(Guid id)
        {
            if (_context == null)
                return NotFound();

            var c = await _context.Classrooms.SingleOrDefaultAsync(m => m.ClassroomId == id);
            if (c == null)
                return NotFound();

            return RedirectToAction("Index");
        }

        // POST: Admin/DeleteClassroom
        [HttpPost]
        [ActionName("DeleteClassroom")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteClassroomConfirmed(Guid id)
        {
            var c = await _context.Classrooms.SingleOrDefaultAsync(m => m.ClassroomId == id);
            _context.Classrooms.Remove(c);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        #endregion

        #region Class

        public IActionResult AddClass()
        {
            return View();
        }

        // POST: Admin/AddClass
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddClass(
            [Bind("ClassName,ShortName,FrequencyType,Semester,Package")] AddClassViewModel addClassViewModel)
        {
            if (ModelState.IsValid)
            {
                var newClass = new Class(addClassViewModel.ClassName,
                    addClassViewModel.ShortName, addClassViewModel.FrequencyType, addClassViewModel.Semester,
                    addClassViewModel.Package);

                var result = _context.Classes.Any(o => o.ClassName == newClass.ClassName);
                if (result)
                    return NoContent();

                _context.Classes.Add(newClass);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return NoContent();
        }

        // GET: Admin/DeleteClass
        public async Task<IActionResult> DeleteClass(Guid id)
        {
            if (_context == null)
                return NotFound();

            var obj = await _context.Classes.SingleOrDefaultAsync(m => m.ClassId == id);
            if (obj == null)
                return NotFound();

            return RedirectToAction("Index");
        }

        // POST: Admin/DeleteClass
        [HttpPost]
        [ActionName("DeleteClass")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteClassConfirmed(Guid id)
        {
            var cl = await _context.Classes.SingleOrDefaultAsync(m => m.ClassId == id);
            _context.Classes.Remove(cl);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        #endregion

        #region Group

        public IActionResult AddGroup()
        {
            return View();
        }

        // POST: Admin/AddGroup
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddGroup(
            [Bind("YearNumber,HalfYearName,GroupNumber")] AddGroupViewModel addGroupViewModel)
        {
            if (ModelState.IsValid)
            {
                var newGroup = new Group(addGroupViewModel.YearNumber, addGroupViewModel.HalfYearName,
                    addGroupViewModel.GroupNumber);

                var result = _context.Groups.Any(g => g.YearNumber == newGroup.YearNumber && g.HalfYearName == newGroup.HalfYearName && g.GroupNumber == newGroup.GroupNumber);
                if (result)
                    return NoContent();

                _context.Add(newGroup);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return NoContent();
        }

        // GET: Admin/DeleteGroup
        public async Task<IActionResult> DeleteGroup(Guid id)
        {
            if (_context == null)
                return NotFound();

            var obj = await _context.Groups.SingleOrDefaultAsync(m => m.GroupId == id);
            if (obj == null)
                return NotFound();

            return RedirectToAction("Index");
        }

        // POST: Admin/DeleteGroup
        [HttpPost]
        [ActionName("DeleteGroup")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteGroupConfirmed(Guid id)
        {
            var obj = await _context.Groups.SingleOrDefaultAsync(m => m.GroupId == id);
            _context.Groups.Remove(obj);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        #endregion

        #region TeacherToClass

        public IActionResult AddTeacherToClass()
        {
            return View();
        }

        // POST: Admin/AddTeacherToClass
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddTeacherToClass([Bind("TeacherName,ClassName")] AddTeacherToClassViewModel addTeacherToClassViewModel)
        {
            if (ModelState.IsValid)
            {
                var newTeacherToClass = new TeacherToClass(Guid.Parse(addTeacherToClassViewModel.TeacherName), Guid.Parse(addTeacherToClassViewModel.ClassName));

                var result = _context.TeachersToClasses.Any(to => to.TeacherId == newTeacherToClass.TeacherId && to.ClassId == newTeacherToClass.ClassId);
                if (result)
                    return NoContent();

                _context.TeachersToClasses.Add(newTeacherToClass);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return NoContent();
        }

        // GET: Admin/DeleteTeacherToClass
        public async Task<IActionResult> DeleteTeacherToClass(Guid id)
        {
            if (_context == null)
                return NotFound();

            var c = await _context.TeachersToClasses.SingleOrDefaultAsync(to => to.TeacherToClassId == id);
            if (c == null)
                return NotFound();

            return RedirectToAction("Index");
        }

        // POST: Admin/DeleteTeacherToClass
        [HttpPost]
        [ActionName("DeleteTeacherToClass")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteTeacherToClassConfirmed(Guid id)
        {
            var c = await _context.TeachersToClasses.SingleOrDefaultAsync(to => to.TeacherToClassId == id);
            _context.TeachersToClasses.Remove(c);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        #endregion

        #region Schedule

        public IActionResult AddSchedule()
        {
            return View();
        }

        // POST: Admin/AddSchedule
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddSchedule(
            [Bind("ClassroomName,ClassName,ClassType,StartSchedule,EndSchedule,GroupName,Day,TeacherName")] AddScheduleViewModel addScheduleViewModel)
        {
            if (ModelState.IsValid)
            {
                var classes = _context.Classes.Where(o => o.ClassId.Equals(Guid.Parse(addScheduleViewModel.ClassName)));
                var groups = _context.Groups.Where(g => g.GroupId.Equals(Guid.Parse(addScheduleViewModel.GroupName)));
                var teachers =_context.Teachers.Where(t => t.TeacherId.Equals(Guid.Parse(addScheduleViewModel.TeacherName)));

                var newSchedule = new Schedule(
                    addScheduleViewModel.ClassroomName,
                    teachers.FirstOrDefault().LastName + " " + teachers.FirstOrDefault().FirstName,
                    groups.FirstOrDefault().YearNumber + groups.FirstOrDefault().HalfYearName + groups.FirstOrDefault().GroupNumber,
                    classes.FirstOrDefault().ClassName, 
                    addScheduleViewModel.ClassType,
                    addScheduleViewModel.StartSchedule, 
                    addScheduleViewModel.EndSchedule, 
                    addScheduleViewModel.Day);

                var result =
                    _context.Schedules.Any(
                        s =>
                            s.Day == newSchedule.Day && s.ClassName == newSchedule.ClassName &&
                            s.ClassType == newSchedule.ClassType && s.GroupName == newSchedule.GroupName &&
                            s.ClassroomName == newSchedule.ClassroomName && s.TeacherName == newSchedule.TeacherName);

                var currentClassSchedule =
                    _context.Schedules.Where(s => s.ClassroomName == addScheduleViewModel.ClassroomName);

                foreach(var item in currentClassSchedule)
                    if(item.Day == addScheduleViewModel.Day && (
                        addScheduleViewModel.StartSchedule > item.StartSchedule && addScheduleViewModel.StartSchedule < item.EndSchedule ||
                        addScheduleViewModel.EndSchedule > item.StartSchedule && addScheduleViewModel.EndSchedule < item.EndSchedule) ||
                        addScheduleViewModel.StartSchedule <= item.StartSchedule && addScheduleViewModel.EndSchedule >= item.EndSchedule)
                        result = true;

                if (newSchedule.StartSchedule >= newSchedule.EndSchedule || result)
                    return NoContent();

                _context.Add(newSchedule);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return NoContent();
        }

        // GET: Admin/DeleteSchedule
        public async Task<IActionResult> DeleteSchedule(Guid id)
        {
            if (_context == null)
                return NotFound();

            var schedule = await _context.Schedules.SingleOrDefaultAsync(m => m.ScheduleId == id);
            if (schedule == null)
                return NotFound();

            return RedirectToAction("Index");
        }

        // POST: Admin/DeleteSchedule
        [HttpPost]
        [ActionName("DeleteSchedule")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteScheduleConfirmed(Guid id)
        {
            var schedule = await _context.Schedules.SingleOrDefaultAsync(m => m.ScheduleId == id);
            _context.Schedules.Remove(schedule);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        #endregion

        #region ExamSchedule

        public IActionResult AddExamSchedule()
        {
            return View();
        }

        // POST: Admin/AddExamSchedule
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddExamSchedule(
            [Bind("ClassroomName,Date,StartExam,EndExam,ClassName,ExamType,TeacherName,GroupName")] AddExamScheduleViewModel addExamScheduleViewModel)
        {
            if (ModelState.IsValid)
            {
                var classes = _context.Classes.Where(o => o.ClassId.Equals(Guid.Parse(addExamScheduleViewModel.ClassName)));
                var groups = _context.Groups.Where(g => g.GroupId.Equals(Guid.Parse(addExamScheduleViewModel.GroupName)));
                var teachers = _context.Teachers.Where(t => t.TeacherId.Equals(Guid.Parse(addExamScheduleViewModel.TeacherName)));

                var newExamSchedule = new ExamSchedule(
                    addExamScheduleViewModel.ClassroomName,
                    addExamScheduleViewModel.Date,
                    addExamScheduleViewModel.StartExam,
                    addExamScheduleViewModel.EndExam,
                    classes.FirstOrDefault().ClassName,
                    addExamScheduleViewModel.ExamType,
                    teachers.FirstOrDefault().LastName + " " + teachers.FirstOrDefault().FirstName,
                    groups.FirstOrDefault().YearNumber + groups.FirstOrDefault().HalfYearName + groups.FirstOrDefault().GroupNumber
                    );

                var result =
                    _context.ExamSchedules.Any(
                        s =>
                            s.Date == newExamSchedule.Date && s.ClassName == newExamSchedule.ClassName &&
                            s.GroupName == newExamSchedule.GroupName &&
                            s.ClassroomName == newExamSchedule.ClassroomName &&
                            s.TeacherName == newExamSchedule.TeacherName &&
                            s.ExamType == newExamSchedule.ExamType);

                var currentClassExamSchedule =
                    _context.ExamSchedules.Where(s => s.ClassroomName == addExamScheduleViewModel.ClassroomName);

                foreach (var item in currentClassExamSchedule)
                    if (item.Date == addExamScheduleViewModel.Date && (
                        addExamScheduleViewModel.StartExam > item.StartExam && addExamScheduleViewModel.StartExam < item.EndExam ||
                        addExamScheduleViewModel.EndExam > item.StartExam && addExamScheduleViewModel.EndExam < item.EndExam) ||
                        addExamScheduleViewModel.StartExam <= item.StartExam && addExamScheduleViewModel.EndExam >= item.EndExam)
                        result = true;

                if (newExamSchedule.StartExam >= newExamSchedule.EndExam || result)
                    return NoContent();

                _context.ExamSchedules.Add(newExamSchedule);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return NoContent();
        }

        // GET: Admin/DeleteExamSchedule
        public async Task<IActionResult> DeleteExamSchedule(Guid id)
        {
            if (_context == null)
                return NotFound();

            var examSchedule = await _context.ExamSchedules.SingleOrDefaultAsync(m => m.ExamScheduleId == id);
            if (examSchedule == null)
                return NotFound();

            return RedirectToAction("Index");
        }

        // POST: Admin/DeleteExamSchedule
        [HttpPost]
        [ActionName("DeleteExamSchedule")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteExamScheduleConfirmed(Guid id)
        {
            var examSchedule = await _context.ExamSchedules.SingleOrDefaultAsync(m => m.ExamScheduleId == id);
            _context.ExamSchedules.Remove(examSchedule);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        #endregion

        #region EventSchedule

        public IActionResult AddEventSchedule()
        {
            return View();
        }

        // POST: Admin/AddEventSchedule
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddEventSchedule(
            [Bind("ClassroomName,Date,StartEvent,EndEvent,EventName,EventType,Organizer,GroupName")] AddEventScheduleViewModel addEventScheduleViewModel)
        {
            if (ModelState.IsValid)
            {
                var groups = _context.Groups.Where(g => g.GroupId.Equals(Guid.Parse(addEventScheduleViewModel.GroupName)));

                var newEventSchedule = new EventSchedule(
                    addEventScheduleViewModel.ClassroomName,
                    addEventScheduleViewModel.Date,
                    addEventScheduleViewModel.StartEvent,
                    addEventScheduleViewModel.EndEvent,
                    addEventScheduleViewModel.EventName,
                    addEventScheduleViewModel.EventType,
                    addEventScheduleViewModel.Organizer,
                    groups.FirstOrDefault().YearNumber + groups.FirstOrDefault().HalfYearName + groups.FirstOrDefault().GroupNumber
                    );

                var result =
                    _context.EventSchedules.Any(
                        s =>
                            s.Date == newEventSchedule.Date && s.EventName == newEventSchedule.EventName &&
                            s.GroupName == newEventSchedule.GroupName &&
                            s.ClassroomName == newEventSchedule.ClassroomName &&
                            s.Organizer == newEventSchedule.Organizer &&
                            s.EventType == newEventSchedule.EventType);

                var currentClassEventSchedule =
                    _context.EventSchedules.Where(s => s.ClassroomName == addEventScheduleViewModel.ClassroomName);

                foreach (var item in currentClassEventSchedule)
                    if (item.Date == addEventScheduleViewModel.Date && (
                        addEventScheduleViewModel.StartEvent > item.StartEvent && addEventScheduleViewModel.StartEvent < item.EndEvent ||
                        addEventScheduleViewModel.EndEvent > item.StartEvent && addEventScheduleViewModel.EndEvent < item.EndEvent) ||
                        addEventScheduleViewModel.StartEvent <= item.StartEvent && addEventScheduleViewModel.EndEvent >= item.EndEvent)
                        result = true;

                if (newEventSchedule.StartEvent >= newEventSchedule.EndEvent || result)
                    return NoContent();

                _context.EventSchedules.Add(newEventSchedule);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return NoContent();
        }

        // GET: Admin/DeleteEventSchedule
        public async Task<IActionResult> DeleteEventSchedule(Guid id)
        {
            if (_context == null)
                return NotFound();

            var eventSchedule = await _context.EventSchedules.SingleOrDefaultAsync(m => m.EventScheduleId == id);
            if (eventSchedule == null)
                return NotFound();

            return RedirectToAction("Index");
        }

        // POST: Admin/DeleteEventSchedule
        [HttpPost]
        [ActionName("DeleteEventSchedule")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteEventScheduleConfirmed(Guid id)
        {
            var eventSchedule = await _context.EventSchedules.SingleOrDefaultAsync(m => m.EventScheduleId == id);
            _context.EventSchedules.Remove(eventSchedule);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        #endregion
    }
}