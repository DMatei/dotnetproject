using System.Linq;
using FiiTT.Core.EnumTypes;
using FiiTT.Infrastructure.Data;
using Microsoft.AspNetCore.Mvc;

namespace FiiTT.Web.Controllers
{
    public class StudentController : Controller
    {
        private readonly ApplicationDbContext _context;

        public StudentController(ApplicationDbContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            if (!new UserTypeValidation().HasRight(UserType.Student))
                return RedirectToAction(nameof(HomeController.Index), "Home");

            ViewBag.GroupsList = _context.Groups.OrderBy(g => g.YearNumber).ThenBy(g => g.HalfYearName).ThenBy(g => g.GroupNumber);
            ViewBag.ScheduleList = _context.Schedules.OrderBy(s => s.Day).ThenBy(s => s.StartSchedule).ThenBy(s => s.EndSchedule).ThenBy(s => s.ClassroomName);
            ViewBag.EventSchedulesList = _context.EventSchedules.OrderBy(e => e.Date).ThenBy(e => e.StartEvent).ThenBy(e => e.EndEvent);
            ViewBag.ExamSchedulesList = _context.ExamSchedules.OrderBy(e => e.Date).ThenBy(e => e.StartExam).ThenBy(e => e.EndExam);

            return View();
        }
    }
}