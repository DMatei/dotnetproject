using System.Linq;
using FiiTT.Core.EnumTypes;
using FiiTT.Infrastructure.Data;
using Microsoft.AspNetCore.Mvc;

namespace FiiTT.Web.Controllers
{
    public class TeacherController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TeacherController(ApplicationDbContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            if (!new UserTypeValidation().HasRight(UserType.Teacher))
                return RedirectToAction(nameof(HomeController.Index), "Home");

            ViewBag.TeachersList = _context.Teachers.OrderBy(t => t.LastName).ThenBy(t => t.FirstName);
            ViewBag.ScheduleList = _context.Schedules.OrderBy(s => s.Day).ThenBy(s => s.StartSchedule).ThenBy(s => s.EndSchedule).ThenBy(s => s.ClassroomName);
            ViewBag.EventSchedulesList = _context.EventSchedules.OrderBy(e => e.Date).ThenBy(e => e.StartEvent).ThenBy(e => e.EndEvent);
            ViewBag.ExamSchedulesList = _context.ExamSchedules.OrderBy(e => e.Date).ThenBy(e => e.StartExam).ThenBy(e => e.EndExam);

            return View();
        }
    }
}