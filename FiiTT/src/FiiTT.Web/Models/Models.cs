﻿using FiiTT.Web.Models.ClassroomViewModels;
using FiiTT.Web.Models.ClassViewModels;
using FiiTT.Web.Models.EventScheduleViewModels;
using FiiTT.Web.Models.ExamScheduleViewModels;
using FiiTT.Web.Models.GroupViewModels;
using FiiTT.Web.Models.ScheduleViewModels;
using FiiTT.Web.Models.TeacherToClassViewModels;
using FiiTT.Web.Models.TeacherViewModels;

namespace FiiTT.Web.Models
{
    public class Models
    {
        public AddTeacherViewModel AddTeacherModel { get; set; }

        public AddScheduleViewModel AddScheduleModel { get; set; }

        public AddClassroomViewModel AddClassroomModel { get; set; }

        public AddClassViewModel AddClassModel { get; set; }

        public AddGroupViewModel AddGroupModel { get; set; }

        public AddEventScheduleViewModel AddEventScheduleModel{ get; set; }

        public AddExamScheduleViewModel AddExamScheduleModel { get; set; }

        public AddTeacherToClassViewModel AddTeacherToClassModel { get; set; }
    }
}