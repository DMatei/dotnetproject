﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using FiiTT.Core.EnumTypes;

namespace FiiTT.Web.Models.ScheduleViewModels
{
    public class AddScheduleViewModel
    {
        [Required]
        [DisplayName("Classroom")]
        public string ClassroomName { get; set; }

        [Required]
        [DisplayName("Teacher")]
        public string TeacherName { get; set; }

        [Required]
        [DisplayName("Group")]
        public string GroupName { get; set; }

        [Required]
        [DisplayName("Class")]
        public string ClassName { get; set; }

        [Required]
        [DisplayName("Class Type")]
        public ClassType ClassType { get; set; }

        [Required]
        [DisplayName("From")]
        [DataType(DataType.Time)]
        public DateTime StartSchedule { get; set; }

        [Required]
        [DisplayName("To")]
        [DataType(DataType.Time)]
        public DateTime EndSchedule { get; set; }

        [Required]
        [DisplayName("Day")]
        public Days Day { get; set; }
    }
}