﻿using System;

namespace FiiTT.Web.Models.ScheduleViewModels
{
    public class RemoveScheduleViewModel
    {
        public Guid RemoveScheduleId { get; set; }
    }
}