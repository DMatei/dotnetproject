﻿using System;

namespace FiiTT.Web.Models.ClassViewModels
{
    public class RemoveClassViewModel
    {
        public Guid RemoveClassId { get; set; }
    }
}