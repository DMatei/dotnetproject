﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using FiiTT.Core.EnumTypes;

namespace FiiTT.Web.Models.ClassViewModels
{
    public class AddClassViewModel
    {
        [DisplayName("Class Name")]
        [Required]
        [StringLength(30)]
        [MinLength(5)]
        public string ClassName { get; set; }

        [DisplayName("Short Name")]
        [Required]
        [MaxLength(10)]
        public string ShortName { get; set; }

        [DisplayName("Frequency")]
        [Required]
        public FrequencyType FrequencyType { get; set; }

        [Range(1, 6)]
        [DisplayName("Semester")]
        [Required]
        public int Semester { get; set; }

        [Range(1, 4)]
        [DisplayName("Optional Package")]
        public int? Package { get; set; }
    }
}