﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace FiiTT.Web.Models.DropdownModels
{
    public class ActionModel
    {
        public ActionModel()
        {
            ActionsList = new List<SelectListItem>();
        }

        public int ActionId { get; set; }
        public IEnumerable<SelectListItem> ActionsList { get; set; }
    }
}