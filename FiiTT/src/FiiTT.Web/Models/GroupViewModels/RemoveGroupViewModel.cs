﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FiiTT.Web.Models.GroupViewModels
{
    public class RemoveGroupViewModel
    {
        [Required]
        public Guid RemoveGroupId { get; set; }
    }
}