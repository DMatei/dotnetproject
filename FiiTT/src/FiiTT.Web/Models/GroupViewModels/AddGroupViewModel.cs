﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FiiTT.Web.Models.GroupViewModels
{
    public class AddGroupViewModel
    {
        [DisplayName("Group Number")]
        public int? GroupNumber { get; set; }

        [DisplayName("Half-year Name")]
        [Required]
        public string HalfYearName { get; set; }

        [DisplayName("Year Number")]
        [Required]
        public int YearNumber { get; set; }
    }
}