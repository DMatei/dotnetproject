﻿using System;

namespace FiiTT.Web.Models.TeacherViewModels
{
    public class RemoveTeacherViewModel
    {
        public Guid RemoveTeacherId { get; set; }
    }
}