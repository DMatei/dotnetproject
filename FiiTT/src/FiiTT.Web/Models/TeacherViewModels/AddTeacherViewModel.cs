﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using FiiTT.Core.EnumTypes;

namespace FiiTT.Web.Models.TeacherViewModels
{
    public class AddTeacherViewModel
    {
        [DisplayName("Teacher Type")]
        [Required]
        public TeacherType TeacherType { get; set; }

        [DisplayName("First Name")]
        [Required]
        [StringLength(20)]
        [MinLength(3)]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        [Required]
        [StringLength(20)]
        [MinLength(3)]
        public string LastName { get; set; }

        [DisplayName("Email Adress")]
        [Required]
        [EmailAddress]
        [Key]
        public string Email { get; set; }
    }
}