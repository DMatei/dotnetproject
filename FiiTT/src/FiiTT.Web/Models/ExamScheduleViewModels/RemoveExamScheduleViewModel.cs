﻿using System;

namespace FiiTT.Web.Models.ExamScheduleViewModels
{
    public class RemoveExamScheduleViewModel
    {
        public Guid RemoveExamScheduleId { get; set; }
    }
}