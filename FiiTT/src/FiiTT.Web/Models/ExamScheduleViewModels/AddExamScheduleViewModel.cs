﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using FiiTT.Core.EnumTypes;

namespace FiiTT.Web.Models.ExamScheduleViewModels
{
    public class AddExamScheduleViewModel
    {
        [Required]
        [DisplayName("Classroom")]
        public string ClassroomName { get; set; }

        [Required]
        [DisplayName("Date")]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [Required]
        [DisplayName("From")]
        [DataType(DataType.Time)]
        public DateTime StartExam{ get; set; }

        [Required]
        [DisplayName("To")]
        [DataType(DataType.Time)]
        public DateTime EndExam { get; set; }

        [Required]
        [DisplayName("Class")]
        public string ClassName { get; set; }

        [Required]
        [DisplayName("Exam Type")]
        public ExamType ExamType{ get; set; }

        [Required]
        [DisplayName("Teacher")]
        public string TeacherName { get; set; }

        [Required]
        [DisplayName("Group")]
        public string GroupName { get; set; }
    }
}