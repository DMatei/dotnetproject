﻿using System;

namespace FiiTT.Web.Models.EventScheduleViewModels
{
    public class RemoveEventScheduleViewModel
    {
        public Guid RemoveEventScheduleId { get; set; }
    }
}