﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using FiiTT.Core.EnumTypes;

namespace FiiTT.Web.Models.EventScheduleViewModels
{
    public class AddEventScheduleViewModel
    {
        [Required]
        [DisplayName("Classroom")]
        public string ClassroomName { get; set; }

        [Required]
        [DisplayName("Date")]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [Required]
        [DisplayName("From")]
        [DataType(DataType.Time)]
        public DateTime StartEvent{ get; set; }

        [Required]
        [DisplayName("To")]
        [DataType(DataType.Time)]
        public DateTime EndEvent { get; set; }

        [Required]
        [StringLength(40)]
        [DisplayName("Event")]
        public string EventName { get; set; }

        [Required]
        [DisplayName("Event Type")]
        public EventType EventType{ get; set; }

        [Required]
        [StringLength(40)]
        [DisplayName("Organizer")]
        public string Organizer { get; set; }

        [Required]
        [DisplayName("Group")]
        public string GroupName { get; set; }
    }
}