﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using FiiTT.Core.EnumTypes;

namespace FiiTT.Web.Models.ClassroomViewModels
{
    public class AddClassroomViewModel
    {
        [DisplayName("Classroom Type")]
        [Required]
        public ClassroomType ClassroomType { get; set; }

        [DisplayName("Classroom Name")]
        [Required]
        [StringLength(20)]
        public string ClassroomName { get; set; }
    }
}