﻿using System;

namespace FiiTT.Web.Models.ClassroomViewModels
{
    public class RemoveClassroomViewModel
    {
        public Guid RemoveClassroomId { get; set; }
    }
}