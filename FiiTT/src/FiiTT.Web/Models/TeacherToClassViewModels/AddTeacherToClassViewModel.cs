﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FiiTT.Web.Models.TeacherToClassViewModels
{
    public class AddTeacherToClassViewModel
    {
        public Guid TeacherToClassId { get; set; }
        [Required]
        [DisplayName("Teacher Name")]
        public string TeacherName { get; set; }

        [Required]
        [DisplayName("Class Name")]
        public string ClassName { get; set; }
    }
}