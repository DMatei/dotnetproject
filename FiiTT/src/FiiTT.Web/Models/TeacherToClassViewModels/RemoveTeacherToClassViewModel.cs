﻿using System;

namespace FiiTT.Web.Models.TeacherToClassViewModels
{
    public class RemoveTeacherToClassViewModel
    {
        public Guid RemoveTeacherToClassId { get; set; }
    }
}