﻿using System;
using System.ComponentModel.DataAnnotations;
using FiiTT.Core.EnumTypes;

namespace FiiTT.Core
{
    public class Classroom
    {
        public Classroom()
        {
        }

        public Classroom(ClassroomType classroomType, string classroomName)
        {
            ClassroomId = Guid.NewGuid();
            ClassroomType = classroomType;
            ClassroomName = classroomName;
        }

        [Key]
        public Guid ClassroomId { get; set; }

        public ClassroomType ClassroomType { get; set; }

        public string ClassroomName { get; set; }
    }
}