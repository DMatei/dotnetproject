﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FiiTT.Core
{
    public class Group
    {
        public Group(int yearNumber, string halfYearName, int? groupNumber)
        {
            GroupId = Guid.NewGuid();
            YearNumber = yearNumber;
            HalfYearName = halfYearName;
            GroupNumber = groupNumber;
        }

        public Group()
        {
        }

        [Key]
        public Guid GroupId { get; set; }

        public int YearNumber { get; set; }

        public string HalfYearName { get; set; }

        public int? GroupNumber { get; set; }
    }
}