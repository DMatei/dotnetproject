﻿using System;
using System.Collections.Generic;

namespace FiiTT.Core.Interfaces
{
    public interface IClassRepository
    {
        void Add(Class c);
        void Edit(Class c);
        void Remove(Guid classId);
        IEnumerable<Class> GetClasses();
        Class FindById(Guid classId);
    }
}