﻿using System.Collections.Generic;

namespace FiiTT.Core.Interfaces
{
    public interface ITeacherRepository
    {
        void Add(Teacher t);
        void Edit(Teacher t);
        void Remove(string email);
        IEnumerable<Teacher> GetTeachers();
        Teacher FindByEmail(string email);
    }
}