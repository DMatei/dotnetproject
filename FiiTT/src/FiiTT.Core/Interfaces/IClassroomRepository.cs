﻿using System;
using System.Collections.Generic;

namespace FiiTT.Core.Interfaces
{
    public interface IClassroomRepository
    {
        void Add(Classroom o);
        void Edit(Classroom o);
        void Remove(Guid classroomId);
        IEnumerable<Classroom> GetClassrooms();
        Classroom FindById(Guid classroomId);
    }
}