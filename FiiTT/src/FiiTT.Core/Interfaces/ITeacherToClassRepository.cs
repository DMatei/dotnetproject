﻿using System;
using System.Collections.Generic;

namespace FiiTT.Core.Interfaces
{
    public interface ITeacherToClassRepository
    {
        void Add(TeacherToClass t);
        void Edit(TeacherToClass t);
        void Remove(Guid teacherToClassId);
        IEnumerable<TeacherToClass> GetTeacherToClasses();
        TeacherToClass FindById(Guid teacherToClassId);
    }
}