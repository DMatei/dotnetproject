﻿using System;
using System.Collections.Generic;

namespace FiiTT.Core.Interfaces
{
    public interface IGroupRepository
    {
        void Add(Group c);
        void Edit(Group c);
        void Remove(Guid groupId);
        IEnumerable<Group> GetGroups();
        Group FindById(Guid classId);
    }
}