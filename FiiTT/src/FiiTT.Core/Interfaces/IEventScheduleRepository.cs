﻿using System;
using System.Collections.Generic;

namespace FiiTT.Core.Interfaces
{
    public interface IEventScheduleRepository
    {
        void Add(EventSchedule e);
        void Edit(EventSchedule e);
        void Remove(Guid eventScheduleId);
        IEnumerable<EventSchedule> GetEventSchedules();
        EventSchedule FindById(Guid eventScheduleId);
    }
}