﻿using System;
using System.Collections.Generic;

namespace FiiTT.Core.Interfaces
{
    public interface IExamScheduleRepository
    {
        void Add(ExamSchedule e);
        void Edit(ExamSchedule e);
        void Remove(Guid examScheduleId);
        IEnumerable<ExamSchedule> GetExamSchedules();
        ExamSchedule FindById(Guid examScheduleId);
    }
}