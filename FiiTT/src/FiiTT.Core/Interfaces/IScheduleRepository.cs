﻿using System;
using System.Collections.Generic;

namespace FiiTT.Core.Interfaces
{
    public interface IScheduleRepository
    {
        void Add(Schedule s);
        void Edit(Schedule s);
        void Remove(Guid scheduleId);
        IEnumerable<Schedule> GetSchedules();
        Schedule FindById(Guid scheduleId);
    }
}