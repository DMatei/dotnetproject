﻿using System;
using System.ComponentModel.DataAnnotations;
using FiiTT.Core.EnumTypes;

namespace FiiTT.Core
{
    public class ExamSchedule
    {
        public ExamSchedule()
        {
        }

        public ExamSchedule(string classroomName, DateTime date, DateTime startExam, DateTime endExam, string className,
            ExamType examType, string teacherName, string groupName)
        {
            ExamScheduleId = Guid.NewGuid();
            ClassroomName = classroomName;
            Date = date;
            StartExam = startExam;
            EndExam = endExam;
            ClassName = className;
            ExamType = examType;
            TeacherName = teacherName;
            GroupName = groupName;
        }

        [Key]
        public Guid ExamScheduleId { get; set; }

        public string ClassroomName { get; set; }

        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [DataType(DataType.Time)]
        public DateTime StartExam { get; set; }

        [DataType(DataType.Time)]
        public DateTime EndExam { get; set; }

        public string ClassName { get; set; }

        public string TeacherName { get; set; }

        public string GroupName { get; set; }

        public ExamType ExamType { get; set; }

    }
}