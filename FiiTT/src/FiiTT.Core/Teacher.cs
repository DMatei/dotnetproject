﻿using System;
using System.ComponentModel.DataAnnotations;
using FiiTT.Core.EnumTypes;

namespace FiiTT.Core
{
    public class Teacher
    {
        public Teacher(string email, TeacherType teacherType, string firstName, string lastName)
        {
            TeacherId = Guid.NewGuid();
            Email = email;
            TeacherType = teacherType;
            FirstName = firstName;
            LastName = lastName;
        }

        public Teacher()
        {
        }

        [Key]
        public Guid TeacherId { get; set; }

        public string Email { get; set; }

        public TeacherType TeacherType { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}