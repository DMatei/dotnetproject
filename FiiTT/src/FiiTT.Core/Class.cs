﻿using System;
using System.ComponentModel.DataAnnotations;
using FiiTT.Core.EnumTypes;

namespace FiiTT.Core
{
    public class Class
    {
        public Class()
        {
        }

        public Class(string className, string shortName, FrequencyType frequencyType, int semester,
            int? package)
        {
            ClassId = Guid.NewGuid();
            ClassName = className;
            ShortName = shortName;
            FrequencyType = frequencyType;
            Semester = semester;
            Package = package;
        }

        [Key]
        public Guid ClassId { get; set; }

        public string ClassName { get; set; }

        public string ShortName { get; set; }

        public FrequencyType FrequencyType { get; set; }

        public int Semester { get; set; }

        public int? Package { get; set; }
    }
}