﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FiiTT.Core
{
    public class TeacherToClass
    {
        public TeacherToClass()
        {
        }

        public TeacherToClass(Guid teacherId, Guid classId)
        {
            TeacherToClassId = Guid.NewGuid();
            TeacherId = teacherId;
            ClassId = classId;
        }

        [Key]
        public Guid TeacherToClassId { get; set; }

        public Guid TeacherId { get; set; }

        public Guid ClassId { get; set; }
    }
}