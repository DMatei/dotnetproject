﻿using System;
using System.ComponentModel.DataAnnotations;
using FiiTT.Core.EnumTypes;

namespace FiiTT.Core
{
    public class EventSchedule
    {
        public EventSchedule()
        {
        }

        public EventSchedule(string classroomName, DateTime date, DateTime startEvent, DateTime endEvent, string eventName,
            EventType eventType, string organizer, string groupName)
        {
            EventScheduleId = Guid.NewGuid();
            ClassroomName = classroomName;
            Date = date;
            StartEvent = startEvent;
            EndEvent = endEvent;
            EventName = eventName;
            EventType = eventType;
            Organizer = organizer;
            GroupName = groupName;
        }

        [Key]
        public Guid EventScheduleId { get; set; }

        public string ClassroomName { get; set; }

        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [DataType(DataType.Time)]
        public DateTime StartEvent { get; set; }

        [DataType(DataType.Time)]
        public DateTime EndEvent { get; set; }

        public string EventName { get; set; }

        public string Organizer { get; set; }

        public string GroupName { get; set; }

        public EventType EventType { get; set; }
    }
}