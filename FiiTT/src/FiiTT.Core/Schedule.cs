﻿using System;
using System.ComponentModel.DataAnnotations;
using FiiTT.Core.EnumTypes;

namespace FiiTT.Core
{
    public class Schedule
    {
        public Schedule()
        {
        }

        public Schedule(string classroomName, string teacherName, string groupName, string className,
            ClassType classType, DateTime startSchedule,
            DateTime endSchedule, Days day)
        {
            ScheduleId = Guid.NewGuid();
            ClassroomName = classroomName;
            TeacherName = teacherName;
            GroupName = groupName;
            ClassName = className;
            ClassType = classType;
            StartSchedule = startSchedule;
            EndSchedule = endSchedule;
            Day = day;
        }

        [Key]
        public Guid ScheduleId { get; set; }

        public string ClassroomName { get; set; }

        public string TeacherName { get; set; }

        public string GroupName { get; set; }

        public string ClassName { get; set; }

        public ClassType ClassType { get; set; }

        [DataType(DataType.Time)]
        public DateTime StartSchedule { get; set; }

        [DataType(DataType.Time)]
        public DateTime EndSchedule { get; set; }

        public Days Day { get; set; }
    }
}