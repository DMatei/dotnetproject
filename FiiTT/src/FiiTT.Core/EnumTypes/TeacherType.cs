﻿namespace FiiTT.Core.EnumTypes
{
    public enum TeacherType
    {
        Asistent,
        Conferentiar,
        Colaborator,
        Lector,
        Profesor,
        Doctorand
    }
}