﻿namespace FiiTT.Core.EnumTypes
{
    public enum ClassroomType
    {
        SalaCurs,
        SalaSeminar,
        SalaLaborator,
        Cabinet,
        Secretariat
    }
}