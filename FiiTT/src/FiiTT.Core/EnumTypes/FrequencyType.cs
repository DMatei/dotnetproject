﻿namespace FiiTT.Core.EnumTypes
{
    public enum FrequencyType
    {
        Saptamanal,
        Pare,
        Impare
    }
}