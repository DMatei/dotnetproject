﻿namespace FiiTT.Core.EnumTypes
{
    public enum EventType
    {
        Conferinta,
        SeminarSpecial,
        Workshop,
        Recuperare
    }
}