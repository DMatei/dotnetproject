﻿using System.ComponentModel;

namespace FiiTT.Core.EnumTypes
{
    public enum Days
    {
        Luni,
        Marți,
        Miercuri,
        Joi,
        Vineri,
        Sâmbătă,
        Duminică
    }
}