﻿namespace FiiTT.Core.EnumTypes
{
    public enum UserType
    {
        Student = 0,
        Teacher = 1,
        Admin = 2
    }
}