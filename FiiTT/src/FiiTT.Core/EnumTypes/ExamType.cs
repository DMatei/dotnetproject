﻿namespace FiiTT.Core.EnumTypes
{
    public enum ExamType
    {
        Examen,
        Partial,
        Proiecte,
        ExamenPractic,
        Restanta
    }
}