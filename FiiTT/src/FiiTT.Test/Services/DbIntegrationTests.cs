﻿using System;
using System.Linq;
using FiiTT.Core;
using FiiTT.Core.EnumTypes;
using FiiTT.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assert = Xunit.Assert;

namespace FiiTT.Test.Services
{
    [TestClass]
    public class DbIntegrationTests : IDisposable
    {
        private readonly ApplicationDbContext _context;

        public DbIntegrationTests()
        {
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkSqlServer()
                .BuildServiceProvider();
            var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
            builder.UseSqlServer(
                    $"Server=(localdb)\\MSSQLLocalDB;Database=FiiTT-DB;Trusted_Connection=True;MultipleActiveResultSets=true")
                .UseInternalServiceProvider(serviceProvider);
            _context = new ApplicationDbContext();
            _context.Database.Migrate();
        }

        

        [TestMethod]
        public void FindClassTest()
        {
            //Add some monsters before querying
            _context.Classes.Add(new Class
            {
                ClassName = "Introducere in .NET",
                ShortName = "NET",
                FrequencyType = FrequencyType.Saptamanal,
                Package = 1,
                Semester = 1
            });
            _context.SaveChanges();
            //FindExam the query
            var query = new DbQueries(_context);
            var classesExecute = query.FindClasses();
            //Verify the results
            Assert.Equal(1, classesExecute.Count());
            var classesVerify = classesExecute.First();
            Assert.Equal("NET", classesVerify.ShortName);
            Assert.Equal(1, classesVerify.Semester);
            Assert.Equal("Introducere in .NET", classesVerify.ClassName);
            Assert.Equal(FrequencyType.Saptamanal, classesVerify.FrequencyType);
            Assert.Equal(1, classesVerify.Package);
        }

        [TestMethod]
        public void FindClassroomTest()
        {
            //Add some monsters before querying
            _context.Classrooms.Add(new Classroom
            {
                ClassroomType = ClassroomType.SalaCurs,
                ClassroomName = "C309"
            });
            _context.SaveChanges();
            //FindExam the query
            var query = new DbQueries(_context);
            var classroomsExecute = query.FindClassrooms();
            //Verify the results
            Assert.Equal(1, classroomsExecute.Count());
            var classroomsVerify = classroomsExecute.First();
            Assert.Equal("C309", classroomsVerify.ClassroomName);
            Assert.Equal(ClassroomType.SalaCurs, classroomsVerify.ClassroomType);
        }

        [TestMethod]
        public void FindEventScheduleTest()
        {
            //Add some monsters before querying
            _context.EventSchedules.Add(new EventSchedule
            {
                EventType = EventType.Conferinta,
                Date = DateTime.Today,
                EndEvent = DateTime.MaxValue,
                StartEvent = DateTime.MinValue,
                GroupName = "A4",
                EventName = "Cloud Computing News",
                Organizer = "Alboaie Lenuta",
                ClassroomName = "C308"
            });
            _context.SaveChanges();
            //FindExam the query
            DbQueries query = new DbQueries(_context);
            var eventScheduleExecute = query.FindEventSchedules();
            //Verify the results
            Assert.Equal(1, eventScheduleExecute.Count());
            var eventScheduleVerify = eventScheduleExecute.First();
            Assert.Equal("A4", eventScheduleVerify.GroupName);
            Assert.Equal(EventType.Conferinta, eventScheduleVerify.EventType);
            Assert.Equal("C308", eventScheduleVerify.ClassroomName);
            Assert.Equal("Cloud Computing News", eventScheduleVerify.EventName);
            Assert.Equal("Alboaie Lenuta", eventScheduleVerify.Organizer);
            Assert.Equal(DateTime.Today, eventScheduleVerify.Date);
            Assert.Equal(DateTime.MinValue, eventScheduleVerify.StartEvent);
            Assert.Equal(DateTime.MaxValue, eventScheduleVerify.EndEvent);
        }

        [TestMethod]
        public void FindExamScheduleTest()
        {
            //Add some monsters before querying
            _context.ExamSchedules.Add(new ExamSchedule
            {
                ClassName = "Machine Learning",
                ExamType = ExamType.Restanta,
                TeacherName = "Ciortuz Liviu",
                StartExam = DateTime.MinValue,
                Date = DateTime.Today,
                EndExam = DateTime.MaxValue,
                GroupName = "A3",
                ClassroomName = "C309"
            });
            _context.SaveChanges();
            //FindExam the query
            DbQueries query = new DbQueries(_context);
            var examScheduleExecute = query.FindExamSchedules();
            //Verify the results
            Assert.Equal(1, examScheduleExecute.Count());
            var examScheduleVerify = examScheduleExecute.First();
            Assert.Equal("Machine Learning", examScheduleVerify.ClassName);
            Assert.Equal(ExamType.Restanta, examScheduleVerify.ExamType);
            Assert.Equal("Ciortuz Liviu", examScheduleVerify.TeacherName);
            Assert.Equal(DateTime.MinValue, examScheduleVerify.StartExam);
            Assert.Equal(DateTime.Today, examScheduleVerify.Date);
            Assert.Equal(DateTime.MaxValue, examScheduleVerify.EndExam);
            Assert.Equal("A3", examScheduleVerify.GroupName);
            Assert.Equal("C309", examScheduleVerify.ClassroomName);
        }
        [TestMethod]
        public void FindGroupTest()
        {
            //Add some monsters before querying
            _context.Groups.Add(new Group {HalfYearName = "A", GroupNumber = 3, YearNumber = 3});
            _context.SaveChanges();
            //FindExam the query
            var query = new DbQueries(_context);
            var groupExecute = query.FindGroups();
            //Verify the results
            Assert.Equal(1, groupExecute.Count());
            var groupVerify = groupExecute.First();
            Assert.Equal("A", groupVerify.HalfYearName);
            Assert.Equal(3, groupVerify.YearNumber);
            Assert.Equal(3, groupVerify.GroupNumber);
        }

        [TestMethod]
        public void FindScheduleTest()
        {
            //Add some monsters before querying
            _context.Schedules.Add(new Schedule
            {
                ClassName = "Machine Learning",
                GroupName = "A3",
                TeacherName = "Ciortuz Liviu",
                ClassroomName = "C309",
                Day = Days.Vineri,
                ClassType = ClassType.Curs,
                StartSchedule = DateTime.MinValue,
                EndSchedule = DateTime.MaxValue
            });
            _context.SaveChanges();
            //FindExam the query
            var query = new DbQueries(_context);
            var scheduleExecute = query.FindSchedules();
            //Verify the results
            Assert.Equal(1, scheduleExecute.Count());
            var scheduleVerify = scheduleExecute.First();
            Assert.Equal("Machine Learning", scheduleVerify.ClassName);
            Assert.Equal("A3", scheduleVerify.GroupName);
            Assert.Equal("Ciortuz Liviu", scheduleVerify.TeacherName);
            Assert.Equal("C309", scheduleVerify.ClassroomName);
            Assert.Equal(Days.Vineri, scheduleVerify.Day);
            Assert.Equal(ClassType.Curs, scheduleVerify.ClassType);
            Assert.Equal(DateTime.MinValue, scheduleVerify.StartSchedule);
            Assert.Equal(DateTime.MaxValue, scheduleVerify.EndSchedule);
        }

        [TestMethod]
        public void FindTeacherTest()
        {
            //Add some monsters before querying
            _context.Teachers.Add(new Teacher
            {
                Email = "vladradulescu@gmail.com",
                LastName = "Radulescu",
                FirstName = "Vlad",
                TeacherType = TeacherType.Lector
            });
            _context.SaveChanges();
            //FindExam the query
            var query = new DbQueries(_context);
            var teacherExecute = query.FindTeachers();
            //Verify the results
            Assert.Equal(1, teacherExecute.Count());
            var teacherVerify = teacherExecute.First();
            Assert.Equal("vladradulescu@gmail.com", teacherVerify.Email);
            Assert.Equal("Radulescu", teacherVerify.LastName);
            Assert.Equal("Vlad", teacherVerify.FirstName);
            Assert.Equal(TeacherType.Lector, teacherVerify.TeacherType);
        }
        public void Dispose()
        {
            _context.Database.EnsureDeleted();
        }
    }
}