﻿using System.Collections.Generic;
using System.Linq;
using FiiTT.Core;
using FiiTT.Core.EnumTypes;
using FiiTT.Infrastructure.Data;

namespace FiiTT.Test.Services
{
    public class DbQueries
    {
        private readonly ApplicationDbContext _context;

        public DbQueries(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Class> FindClasses()
        {
            return _context.Classes
                .Where(c => c.ShortName == "NET");
        }

        public IEnumerable<Classroom> FindClassrooms()
        {
            return _context.Classrooms
                .Where(cr => cr.ClassroomName == "C309");
        }

        public IEnumerable<EventSchedule> FindEventSchedules()
        {
            return _context.EventSchedules
                .Where(e => e.EventName == "Cloud Computing News");
        }

        public IEnumerable<ExamSchedule> FindExamSchedules()
        {
            return _context.ExamSchedules
                .Where(es => es.ExamType == ExamType.Restanta);
        }
        public IEnumerable<Group> FindGroups()
        {
            return _context.Groups
                .Where(g => g.GroupNumber == 3 && g.HalfYearName == "A");
        }

        public IEnumerable<Schedule> FindSchedules()
        {
            return _context.Schedules
                .Where(s => s.ClassName == "Machine Learning");
        }

        public IEnumerable<Teacher> FindTeachers()
        {
            return _context.Teachers
                .Where(t => t.FirstName == "Vlad" && t.LastName == "Radulescu");
        }
    }
}