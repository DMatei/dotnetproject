﻿using System;
using FiiTT.Core;
using FiiTT.Core.EnumTypes;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FiiTT.Test.FiiTT.CoreTests
{
    [TestClass]
    public class EventScheduleTest
    {
        private EventSchedule _eventSchedule;

        [TestInitialize]
        public void EventScheduleInit()
        {
            _eventSchedule = new EventSchedule
            ("C2",
            DateTime.Today,
            DateTime.MinValue,
            DateTime.MaxValue,
            "Sedinta ASII",
            EventType.Workshop,
            "ASII",
            "A5");
        }

        [TestMethod]
        public void When_EventScheduleIsInstanciated_Then_VerifyAll()
        {
            _eventSchedule.EventType.Should().Be(EventType.Workshop);
            _eventSchedule.ClassroomName.Should().Be("C2");
            _eventSchedule.EndEvent.Should().Be(DateTime.MaxValue);
            _eventSchedule.EventName.Should().Be("Sedinta ASII");
            _eventSchedule.StartEvent.Should().Be(DateTime.MinValue);
            _eventSchedule.Date.Should().Be(DateTime.Today);
            _eventSchedule.GroupName.Should().Be("A5");
            _eventSchedule.Organizer.Should().Be("ASII");
        }
    }
}
