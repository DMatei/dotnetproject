﻿using System;
using FiiTT.Core;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FiiTT.Test.FiiTT.CoreTests
{
    [TestClass]
    public class TeacherToClassTest
    {
        private TeacherToClass _teacherToClass;

        [TestInitialize]
        public void TeacherToClassInit()
        {
            _teacherToClass = new TeacherToClass(Guid.NewGuid(), Guid.NewGuid());
        }

        [TestMethod]
        public void When_TeacherToClassIsInstanciated_Then_VerifyAll()
        {
            _teacherToClass.TeacherToClassId.Should().NotBe(Guid.Empty);
            _teacherToClass.ClassId.Should().NotBe(Guid.Empty);
            _teacherToClass.TeacherId.Should().NotBe(Guid.Empty);
        }
    }
}