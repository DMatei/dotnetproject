﻿using FiiTT.Core;
using FiiTT.Core.EnumTypes;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FiiTT.Test.FiiTT.CoreTests
{
    [TestClass]
    public class TeacherTest
    {
        private Teacher _teacher;

        [TestInitialize]
        public void TeacherInit()
        {
            _teacher = new Teacher("asd@asd.asd", TeacherType.Conferentiar, "Liviu", "Ciortuz");
        }

        [TestMethod]
        public void When_TeacherIsInstanciated_Then_VerifyAll()
        {
            _teacher.Email.Should().NotBeNullOrEmpty();
            _teacher.TeacherType.Should().BeOfType<TeacherType>();
            _teacher.FirstName.Should().NotBeNullOrEmpty();
            _teacher.LastName.Should().NotBeNullOrEmpty();
        }
    }
}