﻿using System;
using FiiTT.Core;
using FiiTT.Core.EnumTypes;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FiiTT.Test.FiiTT.CoreTests
{
    [TestClass]
    public class ClassroomTest
    {
        private Classroom _classroom;

        [TestInitialize]
        public void ClassroomInit()
        {
            _classroom = new Classroom(ClassroomType.SalaLaborator, "C403");
        }

        [TestMethod]
        public void When_ClassroomIsInstanciated_Then_VerifyAll()
        {
            _classroom.ClassroomId.Should().NotBe(Guid.Empty);
            _classroom.ClassroomType.Should().BeOfType<ClassroomType>();
            _classroom.ClassroomName.Should().NotBeNullOrEmpty();
        }
    }
}