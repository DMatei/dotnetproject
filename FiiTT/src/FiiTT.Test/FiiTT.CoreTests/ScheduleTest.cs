﻿using System;
using FiiTT.Core;
using FiiTT.Core.EnumTypes;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FiiTT.Test.FiiTT.CoreTests
{
    [TestClass]
    public class ScheduleTest
    {
        private Schedule _schedule;

        [TestInitialize]
        public void ScheduleInit()
        {
            _schedule = new Schedule("C403", "Olariu Florin", "3A3", "Introducere in .NET", ClassType.Laborator,
                new DateTime(2016, 10, 22, 10, 0, 0), new DateTime(2016, 10, 22, 12, 0, 0), Days.Luni);
        }

        [TestMethod]
        public void When_ScheduleIsInstanciated_Then_VerifyAll()
        {
            _schedule.ScheduleId.Should().NotBe(Guid.Empty);
            _schedule.ClassroomName.Should().NotBeNullOrEmpty();
            _schedule.TeacherName.Should().NotBeNullOrEmpty();
            _schedule.GroupName.Should().NotBeNullOrEmpty();
            _schedule.ClassName.Should().NotBeNullOrEmpty();
            _schedule.StartSchedule.Should().BeBefore(_schedule.EndSchedule);
            _schedule.StartSchedule.Should().BeSameDateAs(_schedule.EndSchedule);
        }
    }
}