﻿using System;
using FiiTT.Core;
using FiiTT.Core.EnumTypes;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FiiTT.Test.FiiTT.CoreTests
{
    [TestClass]
    public class ExamScheduleTest
    {
        private ExamSchedule _examSchedule;

        [TestInitialize]
        public void ExamScheduleInit()
        {
            _examSchedule = new ExamSchedule
            (
                "C403",
                DateTime.Today, 
                DateTime.MinValue, 
                DateTime.MaxValue, 
                "Structuri de Date",
                ExamType.Examen, 
                "Gatu Cristian",
                "A3"    
            );
        }
        [TestMethod]
        public void When_ExamScheduleIsInstanciated_Then_VerifyAll()
        {
            _examSchedule.ClassroomName.Should().Be("C403");
            _examSchedule.Date.Should().Be(DateTime.Today);
            _examSchedule.StartExam.Should().Be(DateTime.MinValue);
            _examSchedule.EndExam.Should().Be(DateTime.MaxValue);
            _examSchedule.ClassName.Should().Be("Structuri de Date");
            _examSchedule.ExamType.Should().Be(ExamType.Examen);
            _examSchedule.TeacherName.Should().Be("Gatu Cristian");
            _examSchedule.GroupName.Should().Be("A3");
        }
    }
}
