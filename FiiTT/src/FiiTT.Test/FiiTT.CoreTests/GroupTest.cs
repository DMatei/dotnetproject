﻿using System;
using FiiTT.Core;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FiiTT.Test.FiiTT.CoreTests
{
    [TestClass]
    public class GroupTest
    {
        private Group _group;

        [TestInitialize]
        public void ClassToGroupInit()
        {
            _group = new Group(3, "B", 2);
        }

        [TestMethod]
        public void When_GroupIsInstanciated_Then_VerifyAll()
        {
            _group.GroupId.Should().NotBe(Guid.Empty);
            _group.HalfYearName.Should().NotBeNullOrEmpty();
            _group.GroupNumber.Should().BeGreaterThan(0);
        }
    }
}