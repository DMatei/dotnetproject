﻿using System;
using FiiTT.Core;
using FiiTT.Core.EnumTypes;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FiiTT.Test.FiiTT.CoreTests
{
    [TestClass]
    public class ClassTest
    {
        private Class _class;

        [TestInitialize]
        public void ObjectInit()
        {
            _class = new Class("Introducere in .NET", "I.NET", FrequencyType.Impare, 1, 1);
        }

        [TestMethod]
        public void When_ClassIsInstanciated_Then_VerifyAll()
        {
            _class.ClassId.Should().NotBe(Guid.Empty);
            _class.ClassName.Should().NotBeNullOrEmpty();
            _class.ShortName.Should().NotBeNullOrEmpty();
            _class.ShortName.Length.Should().BeLessThan(_class.ClassName.Length);
            _class.FrequencyType.Should().BeOfType<FrequencyType>();
            _class.Semester.Should().BeInRange(1, 2);
            _class.Package.Should().BeInRange(1, 4);
        }
    }
}